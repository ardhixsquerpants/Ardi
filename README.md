<details>
  <summary><b>My Github Stats</b></summary>
    <img align="center" src="https://github-readme-stats.vercel.app/api?username=ardhixsquerpants&show_icons=true&hide_border=true&hide=issues" alt="Ardhi Ganz github stats">
</details>

<details>
  <summary><b>Connect with me</b></summary>
  <p align="center">
    <i>Let's connect and chat!</i><br><br>
    <a href="https://t.me/ys_hp" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/telegram.svg" alt="@" height="30" width="40" />
    <a href="https://twitter.com/teman_bahagia" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/twitter.svg" alt="teman_bahagia" height="30" width="40" /></a>
    </a>
  </p>
</details>
